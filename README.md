# Bibliothek

A library manager Android App in Kotlin

## Brainstorming
### Tasks to do:
- Find API to gather information based on the ISBN
    - (webpage) https://www.books-by-isbn.com/cgi-bin/isbn-lookup.pl?isbn=[NUMBER]
    - (json) https://www.googleapis.com/books/v1/volumes?q=isbn:[NUMBER]
    - https://books.google.com/books?vid=isbn[NUMBER]
- Design on paper 
- List application functions
- MAKE IT FUNCTIONAL
- TEST IT & REDO
- 

### Primary Functions
- Barcode reader
- Search by ISBN
    - Verify that the code is not already in the digital library
- List of retrieved books from APIs
- List of user’s books
- Book queries by tags
- Tagging books
    - ApplyManual
    - Automatic
    - Custom tags
- Foreign key for landed books
- Sharing libraries
    - Import & Export via text
- Multiple libraries

### Secondary Functions
- Web crawler
- Sharing Libraries wirelessly
- Create Wishlist from a library
- 


### User patterns


### Stories
- User can scanning a barcode to gather the information about a book online
- User can search books online by name
- User can search within their digital library
- User can modify the pre-filled book information
- User can modify the book information at any time 
- User can save/add the books into his digital library
- User can add the books into they Wishlist
- User can mark his books as landed (optional)
    - With a return date 
    - With the name of the browser
    - Date of lend
- User can group and search their Books
    - Author
    - Type (scifi)
    - Edition
    - Series
- User can tag books

### Mockup
![best mockup ever](/docs/images/mockup.jpeg)

# TODO