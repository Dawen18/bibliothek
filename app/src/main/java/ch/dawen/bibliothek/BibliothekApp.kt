package ch.dawen.bibliothek

import android.app.Activity
import android.app.Application
import ch.dawen.bibliothek.di.AppInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import timber.log.Timber
import javax.inject.Inject

/**
 * @author David Wittwer & Joao Mendes
 *
 * @version 0.1
 *
 * Created on 08/11/18 by David Wittwer
 */
class BibliothekApp : Application(), HasActivityInjector {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        AppInjector.init(this)
    }

    override fun activityInjector() = dispatchingAndroidInjector
}