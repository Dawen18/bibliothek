package ch.dawen.bibliothek.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ch.dawen.bibliothek.R
import ch.dawen.bibliothek.db.entity.Book

class BookListAdapter : ListAdapter<Book, BookListAdapter.BookViewHolder>(DIFF_CALLBACK) {
    private lateinit var context: Context

    companion object {

        private val DIFF_CALLBACK: DiffUtil.ItemCallback<Book> =
            object : DiffUtil.ItemCallback<Book>() {
                override fun areItemsTheSame(oldItem: Book, newItem: Book): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(oldItem: Book, newItem: Book): Boolean {
                    //TODO: test ISBN equality instead.
                    return oldItem.name == newItem.name &&
                            oldItem.type == newItem.type
                }
            }
    }

    var onBookClick: (Book) -> Unit = { }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): BookViewHolder {
        context = parent.context

        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.book_item, parent, false)

        return BookViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
        getItem(position)?.let { currentBook: Book ->
            holder.tvIsbn.text = currentBook.isbn
            holder.tvTitle.text = currentBook.name

            holder.tvAuthor.text = context.getString(
                R.string.formattedAuthor,
                currentBook.authorFirstName,
                currentBook.authorLastName
            )

            if (currentBook.series.isNotBlank() && currentBook.subSeries.isNotBlank() && currentBook.number.isNotBlank())
                holder.tvSeries.text = context.getString(
                    R.string.formattedSeries,
                    currentBook.series,
                    currentBook.subSeries,
                    currentBook.number
                )

            holder.onItemClick = { pos ->
                getBookAt(pos)?.let { book: Book ->
                    onBookClick(book)
                }
            }
        }
    }

    fun getBookAt(position: Int): Book? {
        return getItem(position)
    }

    class BookViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val tvTitle: TextView = itemView.findViewById(R.id.tvTitle)
        val tvAuthor: TextView = itemView.findViewById(R.id.tvAuthor)
        val tvSeries: TextView = itemView.findViewById(R.id.tvSeries)
        val tvIsbn: TextView = itemView.findViewById(R.id.tvIsbn)

        var onItemClick: (Int) -> Unit = { }

        init {
            itemView.setOnClickListener {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION)
                    onItemClick(position)
            }
        }
    }

    //Source:
    //This ListAdapter implementation is based on the following CodingInFlow tutorial:
    //https://codinginflow.com/tutorials/android/room-viewmodel-livedata-recyclerview-mvvm/part-10-listadapter
}