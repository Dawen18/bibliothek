package ch.dawen.bibliothek.db

import androidx.room.Database
import androidx.room.RoomDatabase
import ch.dawen.bibliothek.db.dao.BookDao
import ch.dawen.bibliothek.db.dao.BookTypeDao
import ch.dawen.bibliothek.db.dao.LibraryDao
import ch.dawen.bibliothek.db.entity.Book
import ch.dawen.bibliothek.db.entity.BookType
import ch.dawen.bibliothek.db.entity.Library

/**
 * @author David Wittwer & Joao Mendes
 *
 * @version 0.1
 *
 * Created on 11.10.18 by David Wittwer
 */

@Database(entities = [Library::class, Book::class, BookType::class], version = 3)
abstract class BibliothekDB : RoomDatabase() {

    abstract fun libraryDao(): LibraryDao

    abstract fun bookDao(): BookDao

    abstract fun bookTypeDao(): BookTypeDao

}