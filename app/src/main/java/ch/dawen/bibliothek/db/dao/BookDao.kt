package ch.dawen.bibliothek.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ch.dawen.bibliothek.db.entity.Book

/**
 * @author David Wittwer & Joao Mendes
 *
 * @version 0.1
 *
 * Created on 18.10.18 by David Wittwer
 */

@Dao
interface BookDao {
    @Query("SELECT * FROM book")
    fun getAll(): LiveData<List<Book>>

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insert(book: Book)
}