package ch.dawen.bibliothek.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ch.dawen.bibliothek.db.entity.BookType

/**
 * @author David Wittwer & Joao Mendes
 *
 * @version 0.1
 *
 * Created on 18.10.18 by David Wittwer
 */

@Dao
interface BookTypeDao {
    @Query("SELECT * FROM booktype")
    fun getAll(): LiveData<List<BookType>>

    @Insert(onConflict = OnConflictStrategy.FAIL)
    fun insert(book: BookType)
}