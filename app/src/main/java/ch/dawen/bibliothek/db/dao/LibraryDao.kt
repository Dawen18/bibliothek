package ch.dawen.bibliothek.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ch.dawen.bibliothek.db.entity.Library

/**
 * @author David Wittwer & Joao Mendes
 *
 * @version 0.1
 *
 * Created on 11.10.18 by David Wittwer
 */

@Dao
interface LibraryDao {
    @Query("SELECT * FROM library")
    fun getAll(): LiveData<List<Library>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(library: Library)

}