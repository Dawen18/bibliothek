package ch.dawen.bibliothek.db.entity

import androidx.room.PrimaryKey

/**
 * @author David Wittwer & Joao Mendes
 *
 * @version 0.1
 *
 * Created on 01/11/18 by David Wittwer
 */
data class Author(
    @PrimaryKey(autoGenerate = true) val id: Long,
    val firstName: String,
    val lastName: String,
    val type: String
)