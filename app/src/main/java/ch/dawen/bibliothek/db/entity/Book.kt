package ch.dawen.bibliothek.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

/**
 * @author David Wittwer & Joao Mendes
 *
 * @version 0.1
 *
 * Created on 11.10.18 by David Wittwer
 */

@Entity(
    foreignKeys = [
        ForeignKey(
            entity = BookType::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("type")
        ),
        ForeignKey(
            entity = Library::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("library")
        )
    ]
)
data class Book(
    @PrimaryKey(autoGenerate = true) val id: Long,
    @ColumnInfo(index = true) val library: Long,
    @ColumnInfo(index = true) val type: Long,
    val authorFirstName: String,
    val authorLastName: String,
    val editionDate: String,
    val editor: String,
    val gender: String,
    val isbn: String,
    val name: String,
    val number: String,
    val pages: String,
    val series: String,
    val subSeries: String,
    val tag: String
)