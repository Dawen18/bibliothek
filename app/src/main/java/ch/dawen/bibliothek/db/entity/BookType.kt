package ch.dawen.bibliothek.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @author David Wittwer & Joao Mendes
 *
 * @version 0.1
 *
 * Created on 18.10.18 by David Wittwer
 */

@Entity
data class BookType(
    @PrimaryKey(autoGenerate = true) val id: Long,
    val name: String,
    val subSeries: Boolean
)