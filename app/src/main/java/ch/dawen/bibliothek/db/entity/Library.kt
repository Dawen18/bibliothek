package ch.dawen.bibliothek.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @author David Wittwer & Joao Mendes
 *
 * @version 0.1
 *
 * Created on 11.10.18 by David Wittwer
 */

@Entity
data class Library(
    @PrimaryKey(autoGenerate = true) val id: Long,
    @ColumnInfo() val name: String,
    @ColumnInfo() val description: String

) {
    override fun toString(): String {
        return "[$id] - $name, $description "
    }
}