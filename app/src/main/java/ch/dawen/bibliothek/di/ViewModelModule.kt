/*
 * Copyright (C) 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ch.dawen.bibliothek.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ch.dawen.bibliothek.ui.addbook.AddBookViewModel
import ch.dawen.bibliothek.ui.barcode.BarcodeViewModel
import ch.dawen.bibliothek.ui.booklist.BookListViewModel
import ch.dawen.bibliothek.ui.main.MainViewModel
import ch.dawen.bibliothek.viewmodel.BibliothekViewModelFactory
import ch.dawen.bibliothek.viewmodel.BookTypeViewModel
import ch.dawen.bibliothek.viewmodel.LibraryViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(BarcodeViewModel::class)
    abstract fun bindBarcodeViewModel(barcodeViewModel: BarcodeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BookListViewModel::class)
    abstract fun bindBookListViewModel(bookListViewModel: BookListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddBookViewModel::class)
    abstract fun bindAddBookViewModel(searchViewModel: AddBookViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BookTypeViewModel::class)
    abstract fun bindBookTypeViewModel(bookTypeViewModel: BookTypeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LibraryViewModel::class)
    abstract fun bindLibraryViewModel(libraryViewModel: LibraryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(mainViewModel: MainViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: BibliothekViewModelFactory): ViewModelProvider.Factory
}
