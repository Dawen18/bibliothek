package ch.dawen.bibliothek.repository

import android.os.AsyncTask
import androidx.lifecycle.LiveData
import ch.dawen.bibliothek.db.BibliothekDB
import ch.dawen.bibliothek.db.dao.BookDao
import ch.dawen.bibliothek.db.entity.Book
import javax.inject.Inject

/**
 * @author David Wittwer & Joao Mendes
 *
 * @version 0.1
 *
 * Created on 06/01/19 by David Wittwer
 */
class BookRepository @Inject constructor(db: BibliothekDB) {
    private val bookDao = db.bookDao()
    private val allBooks: LiveData<List<Book>>

    init {
        allBooks = bookDao.getAll()
    }

    fun getAllBooks() = allBooks

    fun insert(book: Book) {
        InsertAsyncTask(bookDao).execute(book)
    }

    private class InsertAsyncTask internal constructor(private val dao: BookDao) :
        AsyncTask<Book, Void, Void>() {
        override fun doInBackground(vararg p0: Book?): Void? {
            p0[0]?.let { book ->
                dao.insert(book)
            }

            return null
        }

    }
}