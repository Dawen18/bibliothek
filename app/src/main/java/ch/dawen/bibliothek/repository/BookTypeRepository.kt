package ch.dawen.bibliothek.repository

import android.os.AsyncTask
import androidx.lifecycle.LiveData
import ch.dawen.bibliothek.db.BibliothekDB
import ch.dawen.bibliothek.db.dao.BookTypeDao
import ch.dawen.bibliothek.db.entity.BookType
import javax.inject.Inject

/**
 * @author David Wittwer & Joao Mendes
 *
 * @version 0.1
 *
 * Created on 20.10.18 by David Wittwer
 */
class BookTypeRepository @Inject constructor(db: BibliothekDB) {
    private val bookTypeDao: BookTypeDao = db.bookTypeDao()
    private val allBookType: LiveData<List<BookType>>

    init {
        allBookType = bookTypeDao.getAll()
    }

    fun gelAllBookTypes(): LiveData<List<BookType>> = allBookType

    fun insert(bookType: BookType) {
        InsertAsyncTask(bookTypeDao).execute(bookType)
    }

    private class InsertAsyncTask internal constructor(private val dao: BookTypeDao) :
        AsyncTask<BookType, Void, Void>() {
        override fun doInBackground(vararg p0: BookType?): Void? {
            p0[0]?.let { library ->
                dao.insert(library)
            }

            return null
        }

    }
}