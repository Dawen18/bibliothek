package ch.dawen.bibliothek.repository

import androidx.lifecycle.LiveData
import ch.dawen.bibliothek.db.BibliothekDB
import ch.dawen.bibliothek.db.dao.LibraryDao
import ch.dawen.bibliothek.db.entity.Library
import javax.inject.Inject

/**
 * @author David Wittwer & Joao Mendes
 *
 * @version 0.1
 *
 * Created on 18.10.18 by David Wittwer
 */
class LibraryRepository @Inject constructor(db: BibliothekDB) {
    private val libraryDao: LibraryDao = db.libraryDao()
    private val allLibrary: LiveData<List<Library>>

    init {
        allLibrary = libraryDao.getAll()
    }

    fun getLibraries(): LiveData<List<Library>> = allLibrary

}