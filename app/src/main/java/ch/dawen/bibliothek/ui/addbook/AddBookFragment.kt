package ch.dawen.bibliothek.ui.addbook

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import ch.dawen.bibliothek.R
import ch.dawen.bibliothek.binding.FragmentDataBindingComponent
import ch.dawen.bibliothek.databinding.AddBookFragmentBinding
import ch.dawen.bibliothek.di.Injectable
import ch.dawen.bibliothek.ui.barcode.BarcodeViewModel
import ch.dawen.bibliothek.util.autoCleared
import javax.inject.Inject

class AddBookFragment : Fragment(), Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    private var binding by autoCleared<AddBookFragmentBinding>()

    private lateinit var isbn: String
    private lateinit var viewModel: AddBookViewModel
    private lateinit var scanBarcodeViewModel: BarcodeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.add_book_fragment,
            container,
            false,
            dataBindingComponent
        )


        return binding.root
    }

    private fun setupBindings() {
        binding.model = viewModel
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders
            .of(this, viewModelFactory)
            .get(AddBookViewModel::class.java)

        setupBindings()

        viewModel.getForm().getFields().isbn = isbn

        viewModel.getAdded()
            .observe(this, Observer { added ->
                if (added)
                    navController().navigate(AddBookFragmentDirections.actionToMainFragment())
            })

        binding.root.findViewById<Button?>(R.id.scanBarcodeBtn)?.setOnClickListener {
            navController().navigate(AddBookFragmentDirections.actionScanBarcode())
        }

        scanBarcodeViewModel = ViewModelProviders
            .of(this, viewModelFactory)
            .get(BarcodeViewModel::class.java)

        //Use viewLifecycleOwner instead of "this" to prevent multiple subscriptions.
//        scanBarcodeViewModel.getIsbn().observe(viewLifecycleOwner, Observer { barcode ->
//            if (barcode != null) {
//                viewModel.getForm().getFields().isbn = barcode
//            }
//        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isbn = AddBookFragmentArgs.fromBundle(arguments).isbn
    }

    private fun navController() = findNavController()
}
