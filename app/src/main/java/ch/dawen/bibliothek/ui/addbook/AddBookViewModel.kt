package ch.dawen.bibliothek.ui.addbook

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ch.dawen.bibliothek.repository.BookRepository
import ch.dawen.bibliothek.ui.addbook.model.AddBookForm
import javax.inject.Inject

class AddBookViewModel @Inject constructor(private val bookRepository: BookRepository) :
    ViewModel() {

    private val form: AddBookForm = AddBookForm()
    private val added = MutableLiveData<Boolean>()

    fun getForm() = form

    fun getAdded() = added

    fun onAddClick() {
        if (!form.isValid())
            return

        val book = form.getFields().toBook()

        bookRepository.insert(book)

        added.postValue(true)
    }

}
