package ch.dawen.bibliothek.ui.addbook.model

import ch.dawen.bibliothek.db.entity.Book

/**
 * @author David Wittwer & Joao Mendes
 *
 * @version 0.1
 *
 * Created on 15/11/18 by David Wittwer
 */
class AddBookFields {
    var isbn: String = ""
    var name: String = ""
    var authorFirstName: String = ""
    var authorLastName: String = ""
    var editor: String = ""
    var editionDate: String = ""
    var series: String = ""
    var subSeries: String = ""
    var number: String = ""
    var gender: String = ""
    var tag: String = ""
    var pages: String = ""

    fun toBook() = Book(
        0,
        1,
        1,
        authorFirstName,
        authorLastName,
        editionDate,
        editor,
        gender,
        isbn,
        name,
        number,
        pages,
        series,
        subSeries,
        tag
    )
}