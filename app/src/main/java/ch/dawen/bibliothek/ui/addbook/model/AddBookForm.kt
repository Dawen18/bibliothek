package ch.dawen.bibliothek.ui.addbook.model

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import ch.dawen.bibliothek.BR

/**
 * @author David Wittwer & Joao Mendes
 *
 * @version 0.1
 *
 * Created on 15/11/18 by David Wittwer
 */
class AddBookForm : BaseObservable() {
    private val fields = AddBookFields()

    @Bindable
    fun isValid(): Boolean {
        val valid = isNameValid(false) && isIsbnValid(false) && isPagesValid(false)

        notifyPropertyChanged(BR.valid)

        return valid
    }

    fun getFields() = fields

    fun isNameValid(setMessage: Boolean): Boolean {
        return !fields.name.isEmpty()
    }

    fun isIsbnValid(setMessage: Boolean): Boolean {
        return !fields.isbn.isEmpty()
    }

    fun isPagesValid(setMessage: Boolean): Boolean {
//        TODO: do real tests
        return true
    }
}