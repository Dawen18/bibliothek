package ch.dawen.bibliothek.ui.barcode

import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton
import ch.dawen.bibliothek.R
import com.journeyapps.barcodescanner.BarcodeView
import com.journeyapps.barcodescanner.CaptureActivity
import com.journeyapps.barcodescanner.DecoratedBarcodeView

class BarcodeCaptureActivity : CaptureActivity() {

    private var isTorchOn: Boolean = false
    private var torchBtn: ImageButton? = null
    private var zxingCameraView: BarcodeView? = null
    private var backBtn: ImageButton? = null

    override fun initializeContent(): DecoratedBarcodeView {
        setContentView(R.layout.barcode_capture_activity)
        return findViewById(R.id.zxing_barcode_scanner) ?: super.initializeContent()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        zxingCameraView = findViewById<BarcodeView?>(R.id.zxing_barcode_surface)

        torchBtn = findViewById<ImageButton?>(R.id.torchBtn)?.also {
            it.setOnClickListener {
                val turnOn = !isTorchOn
                isTorchOn = turnOn
                zxingCameraView?.let {
                    // Toggle torch light.
                    it.setTorch(turnOn)
                    // Display the state of the torch light.
                    if (turnOn) {
                        torchBtn?.setImageResource(R.drawable.ic_torch_on)
                    } else {
                        torchBtn?.setImageResource(R.drawable.ic_torch_off)
                    }
                }
            }
        }

        backBtn = findViewById<ImageButton?>(R.id.barcodeBackButton)?.also {
            it.setOnClickListener {
                finish()
            }
        }
    }
}
