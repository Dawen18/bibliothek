package ch.dawen.bibliothek.ui.barcode

import android.os.Looper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BarcodeViewModel @Inject constructor() : ViewModel() {

    private val isbn: MutableLiveData<String> = MutableLiveData()

    fun getIsbn(): LiveData<String> = isbn

    fun setIsbn(value: String) {
        //Check if called on main thread.
        if (Looper.myLooper() == Looper.getMainLooper())
            isbn.value = value
        else
            isbn.postValue(value)
    }
}