package ch.dawen.bibliothek.ui.barcode

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import ch.dawen.bibliothek.R
import ch.dawen.bibliothek.di.Injectable
import com.google.zxing.integration.android.IntentIntegrator
import com.google.zxing.integration.android.IntentResult
import javax.inject.Inject

class ScanBarcodeFragment : Fragment(), Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val CAMERA_PERMISSION_CODE: Int = 0xCA
    private val BARCODE_INTENT_CODE: Int = 0xC0DE
    private val APP_SETTINGS_INTENT_CODE: Int = 0x5E

    enum class PermissionStatus {
        None,
        Accepted,
        RefusedOnce,
        AlwaysRefused,
    }

    private lateinit var scanBarcodeViewModel: BarcodeViewModel
    private var askPermissionPanel: View? = null
    private var askPermissionBtn: Button? = null
    private var gotoSettingsPanel: View? = null
    private var gotoSettingsBtn: Button? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.scan_barcode_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        scanBarcodeViewModel = ViewModelProviders
            .of(this, viewModelFactory)
            .get(BarcodeViewModel::class.java)

        view?.let { v ->
            askPermissionPanel = v.findViewById<View?>(R.id.askPermissionPanel)
            askPermissionBtn = v.findViewById<Button?>(R.id.askPermissionBtn)?.also { bt ->
                bt.setOnClickListener { startOrAskBarcodeReader() }
            }
            gotoSettingsPanel = v.findViewById<View?>(R.id.gotoSettingsPanel)
            gotoSettingsBtn = v.findViewById<Button?>(R.id.gotoSettingsBtn)?.also { bt ->
                bt.setOnClickListener { gotoAppSettings() }
            }
        }

        startOrAskBarcodeReader()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            CAMERA_PERMISSION_CODE ->
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    updateDisplay(PermissionStatus.Accepted)
                    startBarcodeReader()
                } else if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    updateDisplay(PermissionStatus.RefusedOnce)
                } else {
                    updateDisplay(PermissionStatus.AlwaysRefused)
                }
            else ->
                super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            BARCODE_INTENT_CODE -> {
                val barcode = retrieveBarcode(requestCode, resultCode, data)
                if (barcode != null) {
                    // Barcode has been scanned.
                    Toast.makeText(
                        activity,
                        resources.getString(R.string.scannedISBN, barcode),
                        Toast.LENGTH_SHORT
                    ).show()
//                    scanBarcodeViewModel.setIsbn(barcode)
                } else {
                    // Scan has been cancelled.
                    Toast.makeText(
                        activity,
                        resources.getString(R.string.scanCancelled),
                        Toast.LENGTH_SHORT
                    ).show()
                }
                // Close the fragment once the barcode has been scanned or scan cancelled.

//                view?.let { Navigation.findNavController(it).popBackStack() }
                navController().navigate(
                    ScanBarcodeFragmentDirections.actionToAddBook(
                        barcode ?: ""
                    )
                )
            }
            APP_SETTINGS_INTENT_CODE -> {
                if (hasPermission(Manifest.permission.CAMERA)) {
                    //Start the barcode scan, since the permission has been enabled.
                    updateDisplay(PermissionStatus.Accepted)
                    startBarcodeReader()
                }
            }
            else -> {
                super.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    private fun retrieveBarcode(requestCode: Int, resultCode: Int, data: Intent?): String? {
        if (data != null) {
            val intentResult: IntentResult? =
                IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
            return intentResult?.contents
        }
        return null
    }

    private fun hasPermission(permission: String): Boolean {
        return context?.let { ctx ->
            ContextCompat.checkSelfPermission(ctx, permission) == PackageManager.PERMISSION_GRANTED
        } ?: false
    }

    private fun startOrAskBarcodeReader() {
        if (hasPermission(Manifest.permission.CAMERA)) {
            updateDisplay(PermissionStatus.Accepted)
            startBarcodeReader()
        } else {
            requestPermissions(arrayOf(Manifest.permission.CAMERA), CAMERA_PERMISSION_CODE)
        }
    }

    private fun startBarcodeReader() {
        val integrator = IntentIntegrator.forSupportFragment(this)
        if (integrator != null) {
            integrator.setOrientationLocked(true)
            integrator.captureActivity = BarcodeCaptureActivity::class.java
            integrator.setPrompt(getString(R.string.place_barcode_within_window))
            integrator.setDesiredBarcodeFormats(
                IntentIntegrator.EAN_8,
                IntentIntegrator.EAN_13
            )
            integrator.setRequestCode(BARCODE_INTENT_CODE)
            integrator.initiateScan()
        }
    }

    private fun updateDisplay(permissionStatus: PermissionStatus) {
        when (permissionStatus) {
            PermissionStatus.RefusedOnce -> {
                askPermissionPanel?.visibility = View.VISIBLE
                gotoSettingsPanel?.visibility = View.GONE
            }
            PermissionStatus.AlwaysRefused -> {
                askPermissionPanel?.visibility = View.GONE
                gotoSettingsPanel?.visibility = View.VISIBLE
            }
            else -> {
                askPermissionPanel?.visibility = View.GONE
                gotoSettingsPanel?.visibility = View.GONE
            }
        }
    }

    private fun gotoAppSettings() {
        val intent = Intent(
            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
            Uri.fromParts("package", activity?.packageName, null)
        )
        startActivityForResult(intent, APP_SETTINGS_INTENT_CODE)
    }

    /**
     *
     */
    private fun navController() = findNavController()
}