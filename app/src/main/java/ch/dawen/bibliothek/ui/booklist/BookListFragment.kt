package ch.dawen.bibliothek.ui.booklist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ch.dawen.bibliothek.R
import ch.dawen.bibliothek.adapter.BookListAdapter
import ch.dawen.bibliothek.di.Injectable
import javax.inject.Inject

class BookListFragment : Fragment(), Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var bookListViewModel: BookListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.book_list_fragment, container, false)
        createBooksUI(view)
        return view
    }

    private fun createBooksUI(view: View) {
        val booksRecyclerView: RecyclerView = view.findViewById(R.id.booksRecyclerView)
        booksRecyclerView.layoutManager = LinearLayoutManager(this.activity)
        booksRecyclerView.setHasFixedSize(true)

        val booksAdapter = BookListAdapter()
        booksRecyclerView.adapter = booksAdapter

        bookListViewModel = ViewModelProviders
            .of(this, viewModelFactory)
            .get(BookListViewModel::class.java)

        bookListViewModel.getAllBooks().observe(this, Observer { books ->
            if (books != null)
                booksAdapter.submitList(books)
            else
                booksAdapter.submitList(listOf())
        })
    }
}