package ch.dawen.bibliothek.ui.booklist

import androidx.lifecycle.ViewModel
import ch.dawen.bibliothek.repository.BookRepository
import javax.inject.Inject


class BookListViewModel @Inject constructor(private val bookRepository: BookRepository) :
    ViewModel() {

    fun getAllBooks() = bookRepository.getAllBooks()
}