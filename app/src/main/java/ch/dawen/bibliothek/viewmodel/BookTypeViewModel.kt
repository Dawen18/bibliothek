package ch.dawen.bibliothek.viewmodel

import androidx.lifecycle.ViewModel
import ch.dawen.bibliothek.repository.BookTypeRepository
import javax.inject.Inject

/**
 * @author David Wittwer & Joao Mendes
 *
 * @version 0.1
 *
 * Created on 06/01/19 by David Wittwer
 */
class BookTypeViewModel @Inject constructor(private val bookTypeRepository: BookTypeRepository) :
    ViewModel() {

    fun getBookTypes() = bookTypeRepository.gelAllBookTypes()
}