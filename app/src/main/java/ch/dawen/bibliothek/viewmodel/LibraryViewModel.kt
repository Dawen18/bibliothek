package ch.dawen.bibliothek.viewmodel

import androidx.lifecycle.ViewModel
import ch.dawen.bibliothek.repository.LibraryRepository
import javax.inject.Inject

/**
 * @author David Wittwer & Joao Mendes
 *
 * @version 0.1
 *
 * Created on 15/11/18 by David Wittwer
 */
class LibraryViewModel @Inject constructor(private val libraryRepository: LibraryRepository) :
    ViewModel() {

    fun getLibraries() = libraryRepository.getLibraries()
}